<?php

namespace App\Providers;

use App\Services\ApiClient;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register()
    {
        $this->app->singleton(ApiClient::class, function () {
            $client = new ApiClient();
            $client->baseUrl('https://www.anapioficeandfire.com/api/');

            return $client;
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
    }
}
