<?php

namespace App\Http\Controllers;

use App\Services\ApiClient;
use Illuminate\Http\Request;
use App\DataTransferObjects\CharacterDataTransferObjectCollection;

class CharacterController extends Controller
{
    /**
     * @var ApiClient
     */
    private $client;

    /**
     * BookController constructor.
     *
     * @param ApiClient $client
     */
    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    /**
     * Get all characters.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $response = $request->filled('filter')
            ? $this->client->get("characters?gender={$request->query('filter')}&pageSize=50")
            : $this->client->get('characters?pageSize=50');

        $dataTransferObjectCollection = CharacterDataTransferObjectCollection::create($response->json());

        if ($request->filled('sort')) {
            switch ($request->query('order')) {
                case 'desc':
                    $characters = collect($dataTransferObjectCollection)->sortByDesc($request->query('sort'))->values()->all();
                    break;
                default:
                    $characters = collect($dataTransferObjectCollection)->sortBy($request->query('sort'))->values()->all();
            }
            return response()->json(['data' => $characters]);
        }
        return response()->json(['data' => $dataTransferObjectCollection->toArray()]);
    }
}
