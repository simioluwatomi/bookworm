<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Get all comments.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::latest()->paginate(15);

        return response()->json(['data' => $comments]);
    }

    /**
     * Store a newly created comment in the database.
     *
     * @param Request $request
     * @param         $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $id)
    {
        $validatedData = $request->validate([
            'body' => ['required', 'min:15', 'max:500'],
        ]);

        $comment = Comment::create([
            'book_id'              => $id,
            'commenter_ip_address' => $request->ip(),
            'body'                 => $validatedData['body'],
        ]);

        return response()->json([
            'data' => [
                'message' => 'Comment created successfully',
                'comment' => $comment,
            ],
        ], 201);
    }
}
