<?php

namespace App\Http\Controllers;

use App\Services\ApiClient;
use App\DataTransferObjects\BookDataTransferObject;
use App\DataTransferObjects\BookDataTransferObjectCollection;

class BookController extends Controller
{
    /**
     * @var ApiClient
     */
    private $client;

    /**
     * BookController constructor.
     *
     * @param ApiClient $client
     */
    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    public function index()
    {
        $response = $this->client->get('books?pageSize=50');

        $dataTransferObjectCollection = BookDataTransferObjectCollection::create($response->json());

        $books = collect($dataTransferObjectCollection)->sortBy('released_at')->values()->all();

        return response()->json(['data' => $books]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = $this->client->get("books/{$id}");

        $book = BookDataTransferObject::create($response->json());

        return response()->json(['data' => $book->toArray()]);
    }
}
