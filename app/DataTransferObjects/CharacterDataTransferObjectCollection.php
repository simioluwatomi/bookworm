<?php

namespace App\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObjectCollection;

class CharacterDataTransferObjectCollection extends DataTransferObjectCollection
{
    public static function create(array $data): CharacterDataTransferObjectCollection
    {
        $collection = [];

        foreach ($data as $item) {
            $collection[] = CharacterDataTransferObject::create($item);
        }

        return new self($collection);
    }
}
