<?php

namespace App\DataTransferObjects;

use App\Models\Comment;
use Illuminate\Support\Carbon;
use Spatie\DataTransferObject\DataTransferObject;

class BookDataTransferObject extends DataTransferObject
{
    /** @var int|string */
    public $id;

    /** @var string */
    public $url;

    /** @var string */
    public $name;

    /** @var string */
    public $isbn;

    /** @var array */
    public $authors;

    /** @var int */
    public $number_of_pages;

    /** @var string */
    public $publisher;

    /** @var string */
    public $country;

    /** @var string */
    public $media_type;

    /** @var int */
    public $comments_count;

    /** @var array */
    public $characters;

    /** @var array */
    public $pov_characters;

    /** @var null|\Carbon\Carbon */
    public $released_at;

    public static function create($data)
    {
        return new self([
            'id'              => (int) basename($data['url']),
            'url'             => $data['url'],
            'name'            => $data['name'],
            'isbn'            => $data['isbn'],
            'authors'         => $data['authors'],
            'number_of_pages' => $data['numberOfPages'],
            'publisher'       => $data['publisher'],
            'country'         => $data['country'],
            'media_type'      => $data['mediaType'],
            'comments_count'  => Comment::whereBookId(basename($data['url']))->count(),
            'characters'      => $data['characters'],
            'pov_characters'  => $data['povCharacters'],
            'released_at'     => Carbon::parse($data['released']),
        ]);
    }
}
