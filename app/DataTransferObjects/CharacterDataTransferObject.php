<?php

namespace App\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObject;

class CharacterDataTransferObject extends DataTransferObject
{
    /** @var int|string */
    public $id;

    /** @var string */
    public $url;

    /** @var null|string */
    public $name;

    /** @var string */
    public $gender;

    /** @var null|string */
    public $culture;

    /** @var null|array */
    public $titles;

    /** @var null|array */
    public $aliases;

    /** @var null|string */
    public $father;

    /** @var null|string */
    public $mother;

    /** @var null|string */
    public $born;

    /** @var null|string */
    public $died;

    /** @var null|string */
    public $spouse;

    /** @var null|array */
    public $allegiances;

    /** @var array */
    public $books;

    /** @var null|array */
    public $pov_books;

    /** @var null|array */
    public $tv_series;

    /** @var null|array */
    public $played_by;

    public static function create($data)
    {
        return new self([
            'id'          => (int) basename($data['url']),
            'url'         => $data['url'],
            'name'        => empty($data['name']) ? null : $data['name'],
            'gender'      => $data['gender'],
            'culture'     => empty($data['culture']) ? null : $data['culture'],
            'titles'      => empty($data['titles']) ? null : $data['titles'],
            'aliases'     => empty($data['aliases']) ? null : $data['aliases'],
            'father'      => empty($data['father']) ? null : $data['father'],
            'mother'      => empty($data['mother']) ? null : $data['mother'],
            'born'        => empty($data['born']) ? null : $data['born'],
            'died'        => empty($data['died']) ? null : $data['died'],
            'spouse'      => empty($data['spouse']) ? null : $data['spouse'],
            'allegiances' => empty($data['allegiances']) ? null : $data['allegiances'],
            'books'       => $data['books'],
            'pov_books'   => empty($data['povBooks']) ? null : $data['povBooks'],
            'tv_series'   => empty($data['tvSeries']) ? null : $data['tvSeries'],
            'played_by'   => empty($data['playedBy']) ? null : $data['playedBy'],
        ]);
    }
}
