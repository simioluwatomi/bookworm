<?php

namespace App\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObjectCollection;

class BookDataTransferObjectCollection extends DataTransferObjectCollection
{
    public static function create(array $data): BookDataTransferObjectCollection
    {
        $collection = [];

        foreach ($data as $item) {
            $collection[] = BookDataTransferObject::create($item);
        }

        return new self($collection);
    }
}
