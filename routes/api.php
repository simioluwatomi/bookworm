<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\CharacterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::name('book.')->prefix('books')->group(function () {
    Route::get('/', [BookController::class, 'index'])->name('index');
    Route::get('{id}', [BookController::class, 'show'])->name('show');
});

Route::name('comment.')->prefix('comments')->group(function () {
    Route::get('/', [CommentController::class, 'index'])->name('index');
    Route::post('/{id}', [CommentController::class, 'store'])->name('store');
});

Route::name('character.')->prefix('characters')->group(function () {
    Route::get('/{sort?}/{order?}/{filter?}', [CharacterController::class, 'index'])->name('index');
});
