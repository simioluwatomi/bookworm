<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'book_id'              => $faker->numberBetween(1, 12),
        'commenter_ip_address' => $faker->ipv4,
        'body'                 => $faker->text(500),
    ];
});
